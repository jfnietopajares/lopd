package com.hnss.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hnss.entidades.Paciente;
import com.hnss.entidades.Usuario;
import com.hnss.ui.Notificaciones;
import com.hnss.utilidades.Constantes;
import com.hnss.utilidades.Parametros;
import com.jnieto.lopd.MyUI;

public class JimenaDAO {
	private static final Logger logger = LogManager.getLogger(JimenaDAO.class);

	public JimenaDAO() {
	}

	public Connection conecta() {
		Connection conn = null;
		String persistencia = ((String) MyUI.objParametros.get(Parametros.KEY_PERSISTENCIA)).trim();
		if (persistencia.equals(Constantes.MYSQL_STRING)) {
			String dbURL2 = "jdbc:mysql://localhost:8889/hcel";
			String username = "root";
			String password = "root";

			try {
				// Class.forName("oracle.jdbc.OracleDriver");
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection(dbURL2, username, password);
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		if (persistencia.equals(Constantes.ORACLE_STRING)) {
			String dbURL2 = "jdbc:oracle:thin:@10.36.64.164:1525:exhnss01";
			String username = "USR_HCEL";
			String password = "ydda36hU";
			try {
				Class.forName("oracle.jdbc.OracleDriver");
				conn = DriverManager.getConnection(dbURL2, username, password);
				logger.debug("Conexion con bbdd" + dbURL2 + " realizada.");
			} catch (ClassNotFoundException ex) {
				logger.error("Error de conexión con bbdd ", ex);
			} catch (SQLException ex) {
				logger.error("Error de conexión sql con bbdd ", ex);
			}
		}
		return conn;
	}

	public Usuario getUsuario(String dni) {
		String sql;
		Connection conn = this.conecta();
		Usuario usuario = null;
		if (conn != null) {
			sql = " SELECT  * FROM usuarios WHERE userid='" + dni.trim().toUpperCase() + "'";
			try {
				Statement statement = conn.createStatement();
				ResultSet resulSet = statement.executeQuery(sql);
				if (resulSet.next()) {
					usuario = getUsuarioResulset(resulSet);
				}
				statement.close();
				logger.debug(sql);
			} catch (SQLException e1) {
				logger.error(sql, e1);
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
				}
			}
		}
		return usuario;
	}

	public Usuario getUsuarioResulset(ResultSet resulSet) {
		Usuario usuario = null;
		try {
			usuario = new Usuario();
			usuario.setDni(resulSet.getString("userid"));
			usuario.setApellido1(resulSet.getString("apellido1"));
			usuario.setApellido2(resulSet.getString("apellido2"));
			usuario.setNombre(resulSet.getString("nombre"));
			usuario.setMail(resulSet.getString("email"));
			// usuario.setPasswordhash(resulSet.getString("password"));
			usuario.setEstado(resulSet.getInt("estado"));
			usuario.setTelefono(resulSet.getString("telefono"));
			// usuario.setCodcolegiado(resulSet.getString("codcolegiado"));
			// usuario.setPerfil(new Perfil(resulSet.getLong("perfil")));
			// usuario.setCargo(resulSet.getLong("cargo"));
			// usuario.setCategoria(resulSet.getLong("categoria"));
			// usuario.setCsn(resulSet.getString("csn"));
			// usuario.setBusca(resulSet.getString("busca"));
			// usuario.setIp(resulSet.getString("ip"));
			// usuario.setCias(resulSet.getString("cias"));
			// usuario.setSesion(resulSet.getString("sesion"));
			// usuario.setCpf(resulSet.getString("cpf"));
		} catch (SQLException e) {
			logger.error(Notificaciones.SQLERRORRESULSET, e);
		}
		return usuario;
	}

	public Paciente getPaciente(String nhc) {
		String sql;
		Connection conn = this.conecta();
		Paciente paciente = null;
		if (conn != null) {
			sql = " SELECT  p.*,h.nhc FROM pacientes  p " + "JOIN historias h ON h.paciente = p.id " + " WHERE  nhc='"
					+ nhc + "' ";
			try {
				Statement statement = conn.createStatement();
				ResultSet resulSet = statement.executeQuery(sql);
				if (resulSet.next()) {
					paciente = getPacienteResulset(resulSet);
				}
				statement.close();
				logger.debug(sql);
			} catch (SQLException e1) {
				logger.error(sql, e1);
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
				}
			}
		}
		return paciente;
	}

	public Paciente getPacienteResulset(ResultSet resulSet) {
		Paciente paciente = new Paciente();
		try {

			/*
			 * ID APELLIDOSNOMBRE NUMEROHC ESTADO FECHACAMBIO USUCAMBIO IDJIMENA
			 */
			String apellidosnombre = resulSet.getString("ape1").trim() + " " + resulSet.getString("ape2").trim() + ","
					+ resulSet.getString("nombre").trim();
			paciente.setIdJimena(resulSet.getLong("id"));
			paciente.setApellidosnombre(apellidosnombre);
			paciente.setNumerohc(resulSet.getString("nhc"));
			paciente.setEstado(Constantes.BBDD_ACTIVOSI);
			// paciente.setFechacambio(Utilidades.getFechaLocalDate(resulSet.getLong("fechacambio")));
			// paciente.setUsucambio(new
			// UsuarioDAO().getUsuarioDni(resulSet.getString("usucambio"), false));
		} catch (SQLException e) {
			logger.error(Notificaciones.SQLERRORRESULSET, e);
		}
		return paciente;
	}
}
