package com.jnieto.lopd;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.DeploymentConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinServletService;
import com.vaadin.server.VaadinSession;

@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
public class MyUIServlet extends VaadinServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1242022303953140277L;

	protected VaadinServletService createServletService(DeploymentConfiguration deploymentConfiguration)
			throws ServiceException {
		VaadinServletService service = new VaadinServletService(this, deploymentConfiguration) {
			@Override
			public void requestEnd(VaadinRequest request, VaadinResponse response, VaadinSession session) {
				super.requestEnd(request, response, session);
				if (request.getParameter("APL") != null) {
					session.setAttribute("APL", request.getParameter("APL"));
					session.setAttribute("USR", request.getParameter("USR"));
					session.setAttribute("ADDR", request.getParameter("ADDR"));
					session.setAttribute("NHC", request.getParameter("NHC"));
				}
			}
		};
		service.init();
		return service;
	}

	public void init() {
		System.out.println("fadfdad");

	}
}
