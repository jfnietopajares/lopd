package com.jnieto.lopd;

import java.time.LocalDateTime;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hnss.dao.FuncionalidadDAO;
import com.hnss.dao.JimenaDAO;
import com.hnss.dao.PacienteDAO;
import com.hnss.dao.UsuarioDAO;
import com.hnss.entidades.Paciente;
import com.hnss.entidades.Usuario;
import com.hnss.entidades.lopd.LopdIncidencia;
import com.hnss.ui.Menu;
import com.hnss.ui.Notificaciones;
import com.hnss.ui.PantallaLogin;
import com.hnss.ui.lopd.LopdIncidenciaNueva;
import com.hnss.utilidades.Constantes;
import com.hnss.utilidades.Parametros;
import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of an HTML page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LogManager.getLogger(MyUI.class);

	public static Properties objParametros;
	static {
		try {
			objParametros = new Parametros().getParametros();
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}

	final VerticalLayout pantallaPrincipal = new VerticalLayout();
	final HorizontalLayout filamenu = new HorizontalLayout();
	final HorizontalLayout filaformualrios = new HorizontalLayout();

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		this.getReconnectDialogConfiguration().setDialogText(" Cliente desconectado. Intentando reconectar. ");

		setErrorHandler(e -> controlErroresUI(e.getThrowable()));

		Page.getCurrent().setTitle(Constantes.APLICACION_NOMBRE_VENTANA);

		this.setSizeFull();
		pantallaPrincipal.addComponents(filamenu, filaformualrios);
		pantallaPrincipal.setMargin(false);
		pantallaPrincipal.setSpacing(false);
		filamenu.setMargin(false);
		filamenu.setSpacing(false);
		filaformualrios.setMargin(false);
		filaformualrios.setSpacing(false);
		try {
			if (doRemoteLogin(vaadinRequest) == true) {
				LopdIncidencia incidencia = new LopdIncidencia();
				incidencia.setFechaHora(LocalDateTime.now());
				Usuario usuario = (Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME);
				incidencia.setUsuarioRegistra(usuario);
				Paciente paciente = new PacienteDAO().getPacienteNhc(String.valueOf(getSession().getAttribute("NHC")));
				if (paciente != null)
					incidencia.setPaciente(paciente);
				else {
					incidencia.setPaciente(new Paciente(String.valueOf(getSession().getAttribute("NHC"))));
				}
				// incidencia.setUsuarioRegistra(usuario);
				pantallaPrincipal.addComponent(new LopdIncidenciaNueva(incidencia));
			} else {
				pantallaPrincipal.addComponent(new PantallaLogin());
			}
			setContent(pantallaPrincipal);
		} catch (Exception e1) {
			new Notificaciones(e1.getMessage(), true);
		}
	}

	public void showPublicComponent() {
		setContent(new PantallaLogin());
	}

	public void showPrivateComponent(Usuario usuario) {
		/*
		 * final VerticalLayout layout = new VerticalLayout();
		 * layout.addStyleName(ValoTheme.LAYOUT_WELL); layout.setMargin(false);
		 * layout.setSpacing(false); layout.addComponent(new Menu());
		 * setContent(layout);
		 */
		pantallaPrincipal.removeAllComponents();
		pantallaPrincipal.addComponents(filamenu, filaformualrios);
		filamenu.addComponent(new Menu(usuario));
		filaformualrios.removeAllComponents();
	}

	public void actualiza(Component c) {
		pantallaPrincipal.setMargin(false);
		pantallaPrincipal.setSpacing(false);
		filamenu.setMargin(false);
		filamenu.setSpacing(false);
		filaformualrios.setMargin(false);
		filaformualrios.setSpacing(false);
		filaformualrios.removeAllComponents();
		filaformualrios.addComponent(c);
	}

	public void controlErroresUI(Throwable e) {
		logger.error(Notificaciones.ERROR_UI, e);
		new Notificaciones(Notificaciones.ERROR_UI + e.getMessage(), true);
	}

	boolean doRemoteLogin(VaadinRequest request) {
		try {
			String APL = String.valueOf(getSession().getAttribute("APL"));
			String USR = String.valueOf(getSession().getAttribute("USR"));
			String ADDR = String.valueOf(getSession().getAttribute("ADDR"));
			String NHC = String.valueOf(getSession().getAttribute("NHC"));

			if (APL.equals("null")) {
				return false;
			} else if (!APL.isEmpty()) {
				Usuario usuario = new UsuarioDAO().getUsuarioDni(USR, true);
				if (usuario == null) {
					usuario = new JimenaDAO().getUsuario(USR);
					if (usuario != null) {
						Long idLong = new UsuarioDAO().insertaUsuario(usuario);
						if (idLong != null) {
							usuario.setId(idLong);
							usuario.setEstado(Usuario.USUARIO_ACTIVO);
							usuario.setLlamadaExterna(true);
							usuario.setFucionalidadesArrayList(new FuncionalidadDAO().getListaFuncioUsuarioAl(usuario));
						}
					} else {
						new Notificaciones("Usuario no registrado");
						return false;
					}
				} else {
					usuario.setLlamadaExterna(true);
				}
				VaadinSession.getCurrent().setAttribute(Constantes.SESSION_USERNAME, usuario);
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			logger.error("Remote login: " + e);
			return false;
		}

	}
}
